import autograd as ag
import autograd.numpy as np

def f (a,b):
    return a**3 * b - 3*a**2*(b-1) + b**2 - 1

def g (a,b):
    return a**2*b**2 - 2


# Question 1
print("QUESTION 1 : Solutions de F(a,b)=0")
print("Nous avons trouvé graphiquement deux couples (a, b) pour lesquels F(a,b) est égale à 0 :")
print("(a, b) = (%f, %f) \n(a, b) = (%f, %f) \n" %(-2.83, 0.5, -0.75, 1.8))


# Question 2
print("QUESTION 2 : Jacobienne de F")
print("Dans le tp, il est demandé de paramétrer avec a et b mais pour la suite, il est plus pratique de paramétrer avec u.\n")
def Jac_F (u) :
    a = u[0]
    b = u[1]
    return np.array ([[3*a**2 * b - 6*a*(b-1), a**3 - 3*a**2 + 2*b], [2*a*b**2, 2*a**2*b]], dtype=np.float64)


# Question 3

def F(u):
    a = u[0]
    b = u[1]
    return np.array([f(a,b), g(a,b)])

print("QUESTION 3 : Méthode de Newton")

u = np.array([-2.83, 0.5], dtype=np.float64)
for n in range(5):
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-Jac_F(u), F(u))
    u += h
print("Les coordonnées trouvées sont donc précisément les coordonnées des deux zéros de F.")
    
    
# Question 4
print("\nQUESTION 4 : Newton avec jacobian du module autograd")
jacob_F = ag.jacobian(F)
for n in range(5):
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-jacob_F(u), F(u))
    u += h
print("On trouve le même résultat qu'avec la jacobienne calculée à la main.\n")


# Question
def F_5(u) :
    a = u[0]
    b = u[1]
    # Pour f(a,b)
    t1 = a ** 2
    t2 = b ** 2
    t3 = (a*b - 3*b + 3)*t1 + t2 - 1
    # Pour g(a,b)
    t1 = a ** 2
    t2 = b ** 2
    t4 = t1 * t2 - 2
    # Résultat
    return np.array ([t3, t4])

print("QUESTION 5 : Newton avec jacobian d'autograd et F en utilisant le mode forward")
print("On a d'abord rédéfinie une fonction F en mode forward telle qu'indiqué dans le TP.\nVoici le résultat :")
jacob_F = ag.jacobian(F_5)
for n in range(5):
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-jacob_F(u), F_5(u))
    u += h
    

print("\nOn peut également reprendre les questions 3 à 5 avec les autres coordonnées de a et b et ainsi trouver leurs valeurs précisent.\n")

print("Pour (a, b) = (%f, %f) :" %(-0.75, 1.8))

u = np.array([-0.75, 1.8], dtype=np.float64)
print("QUESTION 3 :")
for n in range(5):
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-jacob_F(u), F(u))
    u += h
    
print("\nQUESTION 4 : ")
for n in range(5):
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-jacob_F(u), F(u))
    u += h

print("\nQUESTION 5 : ")
jac_F = ag.jacobian(F_5)
for n in range(5):
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-jac_F(u), F_5(u))
    u += h
    