import autograd as ag
import autograd.numpy as np

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

# Question 1
print("\nQUESTION 1 : Estimation graphique des points stationnaires")
print("Graphiquement on distingue 5 points stationnaires")
print("1. - Un minimum local : (a, b) = ({:.2f}, {:.2f})".format(0.26, 0.9))
print("2. - Un maximum local : (a, b) = ({:.2f}, {:.2f})".format(-1.53, 0.86))
print("Et 3 points col : \n3. - (a, b) = ({:.2f}, {:.2f})".format(-0.18, 1.68))
print("4. - (a, b) = ({:.2f}, {:.2f})".format(-1.24, -0.08))
print("5. - (a, b) = ({:.2f}, {:.2f})".format(0.77, -1.99))


# Question 2
print("\nQUESTION 2 : gradient et matrice Hessienne")

def nabla_f(a,b):
    return np.array ([3*a**2 + 4*a - 2*b + b**3, -2*a + 2*b + 3*a*b**2 - 2], dtype=np.float64)

def H_f(a,b):
    return np.array([[6*a + 4, -2 + 3*b**2],[-2 + 3*b**2, 2 + 6*a*b]], dtype=np.float64)


# Question 3
print("\nQUESTION 3 : Algorithme de Newton sans contrôle du pas")

def Newton(un):
    for i in range(4):
        a = un[0]
        b = un[1]
        print('u[%d] = ' %i, un, 'f(u[%d]) =' %i, f(a,b))
        H_f_un = H_f(a,b)            # Hessienne de f en u[n]
        nabla_f_un = nabla_f(a,b)    # Gradient de f en u[n]
        h = np.linalg.solve(-H_f_un, nabla_f_un)
        un = un + h

print("\nPour le minimum local :")
Newton(np.array([0.26, 0.86], dtype=np.float64))

print("\nPour le maximum local :")
Newton(np.array([-1.53, 0.86], dtype=np.float64))

print("\nPour les points cols :")
Newton(np.array([-0.18, 1.68], dtype=np.float64))
print()
Newton(np.array([-1.24, -0.08], dtype=np.float64))
print()
Newton(np.array([0.77, -1.99], dtype=np.float64))

# Question 4
print("\nQUESTION 4 : Propriétés de la Hessienne\n")
print("Valeurs propres de la Hessienne pour le point 1 :", np.linalg.eigvalsh(H_f(0.225, 0.932)))
print("Pour le point 2 :", np.linalg.eigvalsh(H_f(-1.563, 0.748)))
print("Pour le point 3 :", np.linalg.eigvalsh(H_f(-0.211, 1.566)))
print("Pour le point 4 :", np.linalg.eigvalsh(H_f(-1.238, 0.179)))
print("Pour le point 5 :", np.linalg.eigvalsh(H_f(0.62, -1.962)))

print("\nLorsque toutes les valeurs propres sont positives, c'est un minimum local, lorsqu'elles sont négatives c'est un maximum local et lorsqu'elles sont de signes différent, c'est un col.\nIci, le point stationnaire étant un minimum local est le premier point.")


# Question 5
print("\nQUESTION 5 : Avec les fonctions grad et hessian")

def fbis(u):
    a = u[0]
    b = u[1]
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

def Newton_bis(un):
    for i in range(4):
        print('u[%d] = ' %i, un, 'f(u[%d]) =' %i, fbis(un))
        H_f_un = ag.hessian(fbis)           # Hessienne de f en u[n]
        nabla_f_un = ag.grad(fbis)    # Gradient de f en u[n]
        h = np.linalg.solve(-H_f_un(un), nabla_f_un(un))
        un = un + h

print("\nPour le minimum local :")
Newton_bis(np.array([0.26, 0.86], dtype=np.float64))

print("\nPour le maximum local :")
Newton_bis(np.array([-1.53, 0.86], dtype=np.float64))

print("\nPour les points cols :")
Newton_bis(np.array([-0.18, 1.68], dtype=np.float64))
print()
Newton_bis(np.array([-1.24, -0.08], dtype=np.float64))
print()
Newton_bis(np.array([0.77, -1.99], dtype=np.float64))

print("On obtient les mêmes points donc les valeurs propres de la Hessienne seront les mêmes que dans la question 4.")


# Question 6
print("\nQUESTION 6 : Gradient de f par méthode de dérivation automatique")

def nabla_fter(u):
    a = u[0]
    b = u[1]
    t1 = b**2
    t2 = a**2
    t3 = a*t1*b - 2*a*b +t2*a - 2*b + t1 + 2*t2 + 5
    df_da = 2*a*t1*b + t2 - 2*b
    df_db = a*t1 - 2*a + 2*b
    return np.array([df_da, df_db], dtype=np.float64)
    
def Newton_fter(un):
    for i in range(6):
        print('u[%d] = ' %i, un, 'f(u[%d]) =' %i, fbis(un))
        H_f_un = ag.hessian(fbis)           # Hessienne de f en u[n]
        nabla_f_un = nabla_fter(un)    # Gradient de f en u[n]
        h = np.linalg.solve(-H_f_un(un), nabla_f_un)
        un = un + h

Newton_fter(np.array([0.22, 0.90], dtype=np.float64))

print("\nCela ne semble pas converger vers le point stationnaire souhaité. Peut-être qu'en contrôlant la longueur du pas on pourrait trouver le point souhaité. Il est possible cependant que je me sois trompé dans le code.")
