import autograd as ag
import autograd.numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as nla

# 1. Moindres carrés linéaire
print("\n1 - Moindes carrés linéaire\n")

#Question 1
print("QUESTION 1 : estimation des erreurs")
## Avec les paramêtres choisis
alpha, beta, gamma, mu = 0.5, -2, 1, 7
m = 4

def f(x):
    return alpha * x**3 + beta * x**2 + gamma * x + mu

Tx = np.array([-1.1, 0.17, 1.22, -.5, 2.02, 1.81])    # Points expérimentaux
p = Tx.shape[0]    # p = 6
Ty_sur_la_courbe = np.array([f(x) for x in Tx])
perturbations = 0.5 * np.array([-1.3, 2.7, -5, 0, 1.4, 6])
Ty_experimentaux = Ty_sur_la_courbe + perturbations
print('alpha, beta, gamma, mu] = [%f,%d,%d,%d]' % (alpha, beta, gamma, mu))

erreur_initiale = np.linalg.norm(Ty_sur_la_courbe-Ty_experimentaux)
print("erreur_initiale = ", erreur_initiale)

## Courbe initiale
plt.scatter(Tx, Ty_sur_la_courbe)
xplot = np.linspace(-1.2, 2.1, 50)
yplot = np.array([f(x) for x in xplot])
plt.plot(xplot, yplot, label='Solution initiale')


## Retrouver les valeurs des paramêtres à partir des points expérimentaux
b = Ty_experimentaux
A = np.array([[x**3, x**2, x, 1] for x in Tx])
Q1, R1 = nla.qr(A, mode='economic')
Q1Tb = np.dot(np.transpose(Q1),b)
alpha, beta, gamma, mu = nla.solve_triangular(R1, Q1Tb, lower=False)
print('nouveaux : \n[alpha, beta, gamma, mu] = [%f,%f,%f,%f]' % (alpha, beta, gamma, mu))


new_Ty_sur_la_courbe = np.array([f(x) for x in Tx])
erreur_minimale = np.linalg.norm(new_Ty_sur_la_courbe-Ty_experimentaux)
print("erreur_minimale = ", erreur_minimale)

## courbe optimale
plt.title("Courbes optimale et initiale")
plt.scatter(Tx, new_Ty_sur_la_courbe)
y2plot = np.array([f(x) for x in xplot])
plt.plot(xplot, y2plot, label='Solution optimale')
plt.legend()
plt.show()


# 2. Méthode de Nprint('u[%d] =' %n, u)ewton en une variable
print("\n2 - Méthode de Newton en une variable\n")

#Question 2
print("QUESTION 2 : racine cubique de 2")
def fonction(x):
    return x**3 - 2

def fonctionPrime(x):
    return 3*x**2

# Calcul des premiers termes
u = 2
for n in range(0,7):
    print('u[%d] ='%n, u)
    u = u - fonction(u)/fonctionPrime(u)

print("\nQUESTION 3 : minimum local")
# Question 3
def fprime(x):
    return 3*alpha*x**2 + 2*beta*x + gamma

def fseconde(x):
    return 6*alpha*x + 2*beta

u=2
for n in range(10):
    print('u[%d] ='%n, u)
    u = u - fprime(u)/fseconde(u)

print("\nQUESTION 4 : avec les fonctions grad et hessian")
# Question 4


fprime2 = ag.grad(f)
fseconde2 = ag.grad(fprime)

for n in range(10):
    print('u[%d] ='%n, u)
    u = u - fprime2(u)/fseconde2(u)












