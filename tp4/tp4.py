import autograd as ag
import autograd.numpy as np

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

def nabla_f (a,b) :
    return np.array ([b**3 + 3*a**2 + 4*a - 2*b, 
                      3*a*b**2 - 2*a + 2*b - 2], dtype=np.float64)

def c (a,b) :
    return a**2 + b**2 - 1/2


# Question 4
print("QUESTION 4 : Interprétations graphiques")
print("Le minimum local ne satisfait pas la contrainte. Une contrainte est active si la retirer change tout. Ici la contrainte que l'on a est active.")

# Question 5
print("\nQUESTION 5 : Déterminer les points a et b")
print("a , b = ", 0.175, 0.685)
print("a, b = ", 0.866, -0.5)

# Question 6
def nabla_c(a,b):
    return np.array([2*a, 2*b], dtype=np.float64)

# Question 7
print("\nQUESTION 7 : Lagrangien")
print("Les variables du Lagrangien sont a, b et lambda.")

# Question 8
def Lagrangien(u):
    a, b, lamb = u
    return f(a,b) - lamb * c(a, b)

# Question 9
nabla_Lagrangien = ag.grad(Lagrangien)
H_Lagrangien = ag.hessian(Lagrangien)

# Question 10
print("\nQUESTION 10 : Solution optimale avec Newton")
u = np.array([.22, .685, 0], dtype=np.float64)
for n in range(6):
    H = H_Lagrangien(u)
    g = nabla_Lagrangien(u)
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-H, g)
    u = u + h
    
# Question 11
def objectif(v):
    x1, x2, x3, x4, x5, y1, y2, y3, y4, y5 = v
    return (4000*x1 + 5000*x2 + x3 + x4 + x5 - 8*y1 - 7*y2 - 3*y3 - y4 - y5)**2

# Question 12
def contraintes(v):
    x1, x2, x3, x4, x5, y1, y2, y3, y4, y5 = v
    return np.array([
            2*x1 + x2 + x3 - 8,
            x1 + 2*x2 + x4 - 7,
            x2 + x5 -3,
            8*y1 + 7*y2 + 3*y3 + y4 - 4000,
            y1 + 2*y2 + y3 + y5 - 5000,
            x1*y4,
            x2*y5,
            x3*y1,
            x4*y2,
            x5*y3
        ])
    
# Question 13
print("\nQUESTION 13 : Lagrangien")
print("On aura 10 multiplicateurs de Lagrange, un multiplicateur par contrainte.")    
def lagrangien(u):
    v = u[0:10]
    lambdas = u[10:20]
    return objectif(v) + np.dot(lambdas, contraintes(v))

# Question 14
nabla_L = ag.grad(lagrangien)
H_L = ag.hessian(lagrangien)
u = np.array([3.2, 1.2, 0.2, -0.2, 1.2, 1050, 2020, 0.15, -0.15, 0.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.float64)

for n in range(6):
    H = H_L(u)
    g = nabla_L(u)
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-H, g)
    u = u + h
    
print("\nOn retrouve les bonnes valeurs pour les X mais j'obtiens -3000 et 4000 pour Y1 et Y2 au lieu de 1000 et 2000.")